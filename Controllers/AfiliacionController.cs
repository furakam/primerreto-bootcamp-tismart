using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SeguroMVC.Models;
using SeguroMVC.Models.Dto.Afiliacion;
using Microsoft.EntityFrameworkCore;

namespace SeguroMVC.Controllers
{
    [Route("[controller]")]
    public class AfiliacionController : Controller
    {
        private readonly BdsegurosContext _context;
        private readonly IMapper _mapper;

        public AfiliacionController(IMapper mapper)
        {
            _context = new BdsegurosContext();
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("ListarAfiliaciones")]
        public ActionResult<Response<List<GetTrafiliacion>>> ListarAfiliaciones()
        {
            var response = new Response<List<GetTrafiliacion>>();

            try
            {
                var afiliaciones = _context.Trafiliacions.Include(a => a.Ncliente).Include(a => a.Nseguro).Where(a => a.Cafiliacionestado == true).ToList();

                response.Data = afiliaciones.Select(s => _mapper.Map<GetTrafiliacion>(s)).ToList();
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
            }

            return response;
        }

        [HttpGet("GetAfiliacionById/{id}")]
        public ActionResult<Response<GetFullTrafiliacion>> GetAfiliacionById(int id)
        {
            var response = new Response<GetFullTrafiliacion>();
            try
            {
                var afiliacion = _context.Trafiliacions.Where(a => a.Nafiliacionid == id).FirstOrDefault() ?? throw new Exception("Afiliaciónn no encontrada");

                response.Data = _mapper.Map<GetFullTrafiliacion>(afiliacion);
                response.Message = "Afiliación encontrada exitosamente";
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
                return NotFound(response);
            }

            return response;
        }

        [HttpPost("RegistrarAfiliacion")]
        public ActionResult<Response<string>> RegistrarAfiliacion(CreateTrafiliacion afiliacionForm)
        {
            var response = new Response<string>();
            try
            {
                var seguro = _context.Tmseguros.Where(s => s.Nseguroid == afiliacionForm.Nseguroid).FirstOrDefault() ?? throw new Exception("Seguro no enconrtado");
                var cliente = _context.Tmclientes.Where(c => c.Nclienteid == afiliacionForm.Nclienteid).FirstOrDefault() ?? throw new Exception("Cliente no enconrtado");

                TimeSpan edad = DateTime.Today - cliente.Dclientefechanacimiento;
                int edadEnAnios = (int)edad.TotalDays / 365;

                if (edadEnAnios > seguro.Nseguroedadmaxima)
                {
                    throw new Exception("Supera la edad maxima");
                }

                var formAfiliacion = _mapper.Map<Trafiliacion>(afiliacionForm);
                _context.Trafiliacions.Add(formAfiliacion);
                _context.SaveChanges();

                response.Data = "Registrado con exito";
            }
            catch (Exception error)
            {
                response.Message = error.Message;
                response.Success = false;
                return BadRequest(response);
            }
            return response;
        }

        [HttpPut("ActualizarAfiliacion")]
        public ActionResult<Response<string>> ActualizarAfiliacion(UpdateTrafiliacionDto afiliacionForm)
        {
            var response = new Response<string>();
            try
            {
                // Validar que el registro exista 
                var afiliacionToUpdate = _context.Trafiliacions.Where(a => a.Nafiliacionid == afiliacionForm.Nafiliacionid).FirstOrDefault() ?? throw new Exception($"Afiliación con el id '{afiliacionForm.Nafiliacionid}' no encontrada");

                _mapper.Map(afiliacionForm, afiliacionToUpdate);
                _context.SaveChanges();

                response.Data = "Actualizado exitosamente";
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
                return NotFound(response);
            }
            return response;
        }

        [HttpDelete("DeleteAfiliacionById/{id}")]
        public ActionResult<Response<string>> DeleteAfiliacionById(int id)
        {
            var response = new Response<string>();
            try
            {
                var afiliacion = _context.Trafiliacions.Where(a => a.Nafiliacionid == id).FirstOrDefault() ?? throw new Exception($"La afiliacion con el id {id} no fue entrada");
                afiliacion.Cafiliacionestado = false;
                _context.SaveChanges();
                response.Message = "Eliminado con éxito";
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
                return NotFound(response);
            }
            return response;
        }


    }
}