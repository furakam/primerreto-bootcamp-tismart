using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SeguroMVC.Models;
using SeguroMVC.Models.Dto.Cliente;

namespace SeguroMVC.Controllers
{
    [Route("[controller]")]
    public class ClienteController : Controller
    {
        private readonly BdsegurosContext _context;
        private readonly IMapper _mapper;

        public ClienteController(IMapper mapper)
        {
            _context = new BdsegurosContext();
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("ListarClientes")]
        public ActionResult<List<GetTmclienteDto>> ListarClientes()
        {
            var response = new List<GetTmclienteDto>();

            try
            {
                var clientes = _context.Tmclientes.Where(c => c.Cclienteestado == true).ToList();
                response = clientes.Select(c => _mapper.Map<GetTmclienteDto>(c)).ToList();
                return Ok(response);
            }
            catch (Exception)
            {
                return BadRequest(response);
            }
        }

        [HttpGet("GetClienteById/{id}")]
        public ActionResult<Response<GetFullTmclienteDto>> GetClienteById(int id)
        {
            var response = new Response<GetFullTmclienteDto>();
            try
            {
                var cliente = _context.Tmclientes.Where(c => c.Nclienteid == id).FirstOrDefault() ?? throw new Exception("Cliente no encontrado");

                response.Data = _mapper.Map<GetFullTmclienteDto>(cliente);
                response.Message = "Compañia encontrada exitosamente";
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
                return NotFound(response);
            }

            return response;
        }

        [HttpPost("RegistrarCliente")]
        public ActionResult<Response<string>> RegistrarCliente(CreateTmclienteDto clienteForm)
        {
            var response = new Response<string>();
            Console.WriteLine(clienteForm.Cclientenombres);
            try
            {
                Console.WriteLine(clienteForm.Ntipodocumento);
                //Validando que no se repita el DNI
                var clienteByDni = _context.Tmclientes.Where(c => c.Cclientedocumentoidentidad.Equals(clienteForm.Cclientedocumentoidentidad)).FirstOrDefault();

                if (clienteByDni != null) throw new Exception($"El cliente con DNI '{clienteForm.Cclientedocumentoidentidad}' ya existe");

                var formToCliente = _mapper.Map<Tmcliente>(clienteForm);
                _context.Tmclientes.Add(formToCliente);
                _context.SaveChanges();

                response.Data = "Registrado con exito";
            }
            catch (Exception error)
            {
                Console.WriteLine(error.ToString());
                Console.WriteLine(error.InnerException?.Message);
                response.Message = error.Message;
                response.Success = false;
                return BadRequest(response);
            }
            return response;
        }

        [HttpPut("ActualizarCliente")]
        public ActionResult<Response<string>> ActualizarCliente(UpdateTmclienteDto clienteForm)
        {
            var response = new Response<string>();
            try
            {
                // Validar que el registro exista 
                var clienteToUpdate = _context.Tmclientes.Where(c => c.Nclienteid == clienteForm.Nclienteid).FirstOrDefault() ?? throw new Exception($"Clietne con el id '{clienteForm.Nclienteid}' no encontrado");

                _mapper.Map(clienteForm, clienteToUpdate);
                _context.SaveChanges();

                response.Data = "Actualizado exitosamente";
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
                return NotFound(response);
            }
            return response;
        }

        [HttpDelete("DeleteClienteById/{id}")]
        public ActionResult<Response<string>> DeleteClienteById(int id)
        {
            var response = new Response<string>();
            try
            {
                var cliente = _context.Tmclientes.Where(c => c.Nclienteid == id).FirstOrDefault() ?? throw new Exception($"El cliente con el id {id} no fue entrada");
                cliente.Cclienteestado = false;
                _context.SaveChanges();
                response.Message = "Eliminado con éxito";
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message=error.Message;
                return NotFound(response);
            }
            return response;
        }

    }
}