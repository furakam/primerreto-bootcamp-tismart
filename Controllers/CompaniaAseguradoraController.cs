using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SeguroMVC.Models;
using SeguroMVC.Models.Dto.CompaniaAseguradora;

namespace SeguroMVC.Controllers
{
    [Route("[controller]")]
    public class CompaniaAseguradoraController : Controller
    {
        private readonly BdsegurosContext _context;
        private readonly IMapper _mapper;

        public CompaniaAseguradoraController(IMapper mapper)
        {
            _context = new BdsegurosContext();
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("ListarCompaniaAseguradora")]
        public ActionResult<List<GetTmcompaniaAseguradoraDto>> ListarCompaniaAseguradora()
        {
            var response = new List<GetTmcompaniaAseguradoraDto>();
            try
            {
                var companiasAseguradoras = _context.TmcompaniaAseguradoras.Where(ca => ca.Ccompaniaestado == true).ToList();

                response = companiasAseguradoras.Select(ca => _mapper.Map<GetTmcompaniaAseguradoraDto>(ca)).ToList();

                return Ok(response);
            }
            catch (Exception)
            {
                return BadRequest(response);
            }
        }

        [HttpPost("RegistrarCompaniaAseguradora")]
        public ActionResult<Response<string>> RegistrarCompaniaAseguradora(CreateTmcompaniaAseguradoraDto companiaForm)
        {
            var response = new Response<string>();
            try
            {
                //Validando que no se repita el RUC
                var companiaByRuc = _context.TmcompaniaAseguradoras.Where(c => c.Ccompaniaruc.Equals(companiaForm.Ccompaniaruc)).FirstOrDefault();
                Console.WriteLine(companiaByRuc);

                if (companiaByRuc != null) throw new Exception($"La compañía con ruc '{companiaForm.Ccompaniaruc}' ya existe");

                var formToCompania = _mapper.Map<TmcompaniaAseguradora>(companiaForm);
                _context.TmcompaniaAseguradoras.Add(formToCompania);
                _context.SaveChanges();

                response.Data = "Registrado con exito";
            }
            catch (Exception error)
            {
                response.Message = error.Message;
                response.Success = false;
                return BadRequest(response);
            }
            return response;
        }

        [HttpPut("ActualizarCompaniaAseguradora")]
        public ActionResult<Response<string>> ActualizarCompaniaAseguradora(UpdateTmcompaniaAseguradoraDto companiaForm)
        {
            var response = new Response<string>();
            try
            {
                // Validar que el registro exista 
                var companiaToUpdate = _context.TmcompaniaAseguradoras.Where(c => c.Ncompaniaid == companiaForm.Ncompaniaid).FirstOrDefault() ?? throw new Exception($"Compañía con el id '{companiaForm.Ncompaniaid}' no encontrada");

                _mapper.Map(companiaForm, companiaToUpdate);
                _context.SaveChanges();

                response.Data = "Actualizado exitosamente";
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
                return NotFound(response);
            }
            return response;
        }

        [HttpGet("GetCompaniaAseguradoraById/{id}")]
        public ActionResult<Response<GetFullTmcompaniaAseguradoraDto>> GetCompaniaAseguradoraById(int id)
        {
            var response = new Response<GetFullTmcompaniaAseguradoraDto>();
            try
            {
                var compania = _context.TmcompaniaAseguradoras.Where(c => c.Ncompaniaid == id).FirstOrDefault() ?? throw new Exception("Compañía no encontrada");

                response.Data = _mapper.Map<GetFullTmcompaniaAseguradoraDto>(compania);
                response.Message = "Compañia encontrada exitosamente";
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
                return NotFound(response);
            }

            return response;
        }

        [HttpDelete("DeleteCompaniaAseguradoraById/{id}")]
        public ActionResult<Response<string>> DeleteCompaniaAseguradoraById(int id)
        {
            var response = new Response<string>();
            try
            {
                var compania = _context.TmcompaniaAseguradoras.Where(c => c.Ncompaniaid == id).FirstOrDefault() ?? throw new Exception($"La compañía con el id {id} no fue entrada");
                compania.Ccompaniaestado = false;
                _context.SaveChanges();
                response.Message = "Eliminado con éxito";
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message=error.Message;
                return NotFound(response);
            }
            return response;
        }
    }
}