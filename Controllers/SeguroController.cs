using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeguroMVC.Models;
using SeguroMVC.Models.Dto.Seguro;

namespace SeguroMVC.Controllers
{
    [Route("[controller]")]
    public class SeguroController : Controller
    {
        private readonly BdsegurosContext _context;
        private readonly IMapper _mapper;

        public SeguroController(IMapper mapper)
        {
            _context = new BdsegurosContext();
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("GetSeguroById/{id}")]
        public ActionResult<Response<GetFullTmsegurodto>> GetSeguroById(int id)
        {
            var response = new Response<GetFullTmsegurodto>();
            try
            {
                var seguro = _context.Tmseguros.Where(s => s.Nseguroid == id).FirstOrDefault() ?? throw new Exception("Seguro no encontrado");

                response.Data = _mapper.Map<GetFullTmsegurodto>(seguro);
                response.Message = "Seguro encontrado exitosamente";
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
                return NotFound(response);
            }

            return response;
        }

        [HttpGet("ListarSeguros")]
        public ActionResult<Response<List<GetTmseguroDto>>> ListarSeguros()
        {
            var response = new Response<List<GetTmseguroDto>>();

            try
            {
                var seguros = _context.Tmseguros.Include(s => s.Ncompania).Where(s => s.Cseguroestado == true).ToList();

                response.Data = seguros.Select(s => _mapper.Map<GetTmseguroDto>(s)).ToList();
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
            }

            return response;
        }

        [HttpPost("RegistrarSeguro")]
        public ActionResult<Response<string>> RegistrarSeguro(CreateTmseguroDto seguroForm)
        {
            var response = new Response<string>();
            try
            {
                var formToSeguro = _mapper.Map<Tmseguro>(seguroForm);
                _context.Tmseguros.Add(formToSeguro);
                _context.SaveChanges();

                response.Data = "Registrado con exito";
            }
            catch (Exception error)
            {
                response.Message = error.Message;
                response.Success = false;
                return BadRequest(response);
            }
            return response;
        }

        [HttpPut("ActualizarSeguro")]
        public ActionResult<Response<string>> ActualizarSeguro(UpdateTmseguroDto seguroForm)
        {
            var response = new Response<string>();
            try
            {
                // Validar que el registro exista 
                var seguroToUpdate = _context.Tmseguros.Where(s => s.Nseguroid == seguroForm.Nseguroid).FirstOrDefault() ?? throw new Exception($"Seguro con el id '{seguroForm.Nseguroid}' no encontrado");

                _mapper.Map(seguroForm, seguroToUpdate);
                _context.SaveChanges();

                response.Data = "Actualizado exitosamente";
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
                return NotFound(response);
            }
            return response;
        }

        [HttpDelete("DeleteSeguroById/{id}")]
        public ActionResult<Response<string>> DeleteSeguroById(int id)
        {
            var response = new Response<string>();
            try
            {
                var seguro = _context.Tmseguros.Where(s => s.Nseguroid == id).FirstOrDefault() ?? throw new Exception($"El seguro con el id {id} no fue entrada");
                seguro.Cseguroestado = false;
                _context.SaveChanges();
                response.Message = "Eliminado con éxito";
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message=error.Message;
                return NotFound(response);
            }
            return response;
        }

    }
}