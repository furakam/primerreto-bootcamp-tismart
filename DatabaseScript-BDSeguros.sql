USE [master]
GO

/* ALTER DATABASE [BDSeguros] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
GO */

DROP DATABASE [BDSeguros]
GO

CREATE DATABASE [BDSeguros]
GO

USE [BDSeguros]
GO

CREATE TABLE [TMCompaniaAseguradora]
(
    [ncompaniaid] INT IDENTITY(1,1) PRIMARY KEY,
    [ccompaniadescripcion] NVARCHAR(1000) NOT NULL,
    [ccompaniaruc] CHAR(11) NOT NULL,
    [ccompaniarazonsocial] NVARCHAR(400) NOT NULL,
    [ccompaniacontacto] CHAR(9) NOT NULL,
    [ccompaniacelular] CHAR(9) NOT NULL,
    [ccompaniacontrato] NVARCHAR(MAX),
    [dcompaniafecharenovacion] DATETIME NOT NULL,
    [ccompaniaestado] BIT DEFAULT 1,
    [cusercreate] NVARCHAR(50) DEFAULT 'DEFAULT_USER',
    [dusercreate] DATETIME DEFAULT GETDATE() NOT NULL,
    [cuserupdate] NVARCHAR(50) DEFAULT 'DEFAULT_USER',
    [duserupdate] DATETIME,
)
GO

CREATE TABLE [TMSeguro]
(
    [nseguroid] INT IDENTITY(1,1) PRIMARY KEY,
    [ncompaniaid] INT NOT NULL,
    [cseguronumero] VARCHAR(10) NOT NULL,
    [ntiposeguro] VARCHAR(250) NOT NULL,
    [csegurodescripcion] NVARCHAR(1000) NOT NULL,
    [nsegurofactorimpuesto] INT,
    [nseguroporcentajecomision] INT NOT NULL,
    [nseguromontoprima] MONEY NOT NULL,
    [nmonedaid] VARCHAR(50) NOT NULL,
    [nseguroedadmaxima] INT NOT NULL,
    [dsegurofechavigencia] DATETIME NOT NULL,
    [nseguroimportemensual] MONEY NOT NULL,
    [nsegurocobertura] MONEY NOT NULL,
    [cseguroestado] BIT DEFAULT 1,
    [cusercreate] NVARCHAR(50) DEFAULT 'DEFAULT_USER',
    [dusercreate] DATETIME DEFAULT GETDATE() NOT NULL,
    [cuserupdate] NVARCHAR(50) DEFAULT 'DEFAULT_USER',
    [duserupdate] DATETIME
)
GO

CREATE TABLE [TMCliente]
(
    [nclienteid] INT IDENTITY(1,1) PRIMARY KEY,
    [ntipodocumento] VARCHAR(20) NOT NULL,
    [cclientedocumentoidentidad] VARCHAR(20) NOT NULL,
    [cclientegenero] CHAR(1) NOT NULL,
    [cclienteapellidopaterno] NVARCHAR(50) NOT NULL,
    [cclienteapellidomaterno] NVARCHAR(50) NOT NULL,
    [cclientenombres] NVARCHAR(100) NOT NULL,
    [cclienteemail] NVARCHAR(100) NOT NULL,
    [cclientetelefono] CHAR(9) NOT NULL,
    [cclientedireccion] NVARCHAR(200) NOT NULL,
    [dclientefechanacimiento] DATE NOT NULL,
    [cclienteestado] BIT DEFAULT 1,
    [cusercreate] NVARCHAR(50) DEFAULT 'DEFAULT_USER',
    [dusercreate] DATETIME DEFAULT GETDATE() NOT NULL,
    [cuserupdate] NVARCHAR(50) DEFAULT 'DEFAULT_USER',
    [duserupdate] DATETIME
)
GO

CREATE TABLE [TRAfiliacion]
(
    [nafiliacionid] INT IDENTITY(1,1) PRIMARY KEY,
    [nclienteid] INT NOT NULL,
    [nseguroid] INT NOT NULL,
    [dsegurofechaafiliación] DATETIME DEFAULT GETDATE(),
    [cafiliacionestado] BIT DEFAULT 1,
    [cusercreate] NVARCHAR(50) DEFAULT 'DEFAULT_USER',
    [dusercreate] DATETIME DEFAULT GETDATE() NOT NULL,
    [cuserupdate] NVARCHAR(50) DEFAULT 'DEFAULT_USER',
    [duserupdate] DATETIME
)
GO


-- Foreign key contstraints

ALTER TABLE [TMSeguro] ADD CONSTRAINT [FK_Ncompaniaid] FOREIGN KEY ([ncompaniaid]) REFERENCES [TMCompaniaAseguradora]([ncompaniaid])
GO

ALTER TABLE [TRAfiliacion] ADD CONSTRAINT [FK_Nclienteid] FOREIGN KEY ([nclienteid]) REFERENCES [TMCliente]([nclienteid])
GO

ALTER TABLE [TRAfiliacion] ADD CONSTRAINT [FK_Nseguroid] FOREIGN KEY ([nseguroid]) REFERENCES [TMSeguro]([nseguroid])
GO

-- Insert some data in tables

-- -> TMCompaniaAseguradora
INSERT INTO [TMCompaniaAseguradora]
    (
        [ccompaniadescripcion],
        [ccompaniaruc],
        [ccompaniarazonsocial],
        [ccompaniacontacto],
        [ccompaniacelular],
        [dcompaniafecharenovacion]
    )
VALUES
    (
        'Empresa de seguros con presencia en Perú y parte del Grupo Mapfre. Ofrece una amplia gama de productos y servicios de seguros para individuos y empresas, incluyendo seguros de automóviles, hogar, vida, salud, responsabilidad civil, comercio y transporte. Su enfoque se centra en brindar protección financiera y tranquilidad a sus clientes, con énfasis en la calidad del servicio y la atención personalizada. Además de los seguros, también brinda servicios de asistencia. Mapfre Perú se destaca por su trayectoria y experiencia, así como por su compromiso con la ética y la responsabilidad social. Cuenta con una red de sucursales y agentes en todo el país para brindar un servicio cercano y accesible.',
        '12238728372',
        'Mapfre Perú Compañía de Seguros y Reaseguros S.A.',
        '017837837',
        '987898379',
        '2024-07-14'
    ),
    (
        'Pacífico Compañía de Seguros y Reaseguros es una compañía líder en Perú, parte del Grupo Credicorp. Ofrece una variedad de seguros para individuos y empresas, incluyendo vida, salud, automóviles y hogar. Con una sólida reputación, se enfoca en brindar protección adaptada a las necesidades de sus clientes, con servicios de calidad y compromiso con el bienestar.',
        '12238728372',
        'Pacífico Compañía de Seguros y Reaseguros',
        '017837432',
        '939849839',
        '2024-07-14'
    )
GO

-- --> Tabla TMSeguro
INSERT INTO [TMSeguro]
    (
        [ncompaniaid],
        [cseguronumero],
        [ntiposeguro],
        [csegurodescripcion],
        [nsegurofactorimpuesto],
        [nseguroporcentajecomision],
        [nseguromontoprima],
        [nmonedaid],
        [nseguroedadmaxima],
        [dsegurofechavigencia],
        [nseguroimportemensual],
        [nsegurocobertura]
    )
VALUES
    (1, 'SG001', 'Vida', 'Seguro de vida para adultos', 10, 5, 1000.00, 'PEN', 65, '2023-08-01', 50.00, 500000.00),
    (2, 'SG002', 'Auto', 'Seguro de automóvil a todo riesgo', 5, 8, 500.00, 'PEN', 70, '2023-09-15', 80.00, 250000.00)
GO

-- --> Tabla TMCliente
INSERT INTO [TMCliente] 
    (
        [ntipodocumento], 
        [cclientedocumentoidentidad], 
        [cclientegenero], 
        [cclienteapellidopaterno], 
        [cclienteapellidomaterno], 
        [cclientenombres], 
        [cclienteemail],
        [cclientetelefono],
        [cclientedireccion],
        [dclientefechanacimiento]
    )
VALUES
    ('DNI', '12345678', 'M', 'García', 'López', 'Juan', 'juangarcia@example.com', '987654321', 'Av. Lima 123', '1990-01-15'),
    ('RUC', '87654321', 'F', 'Martínez', 'Sánchez', 'María', 'mariamartinez@example.com', '999888777', 'Jr. Arequipa 456', '1985-08-27')
GO


-- --> Table TRAfiliacion
INSERT INTO [TRAfiliacion] 
(
    [nclienteid],
    [nseguroid], 
    [dsegurofechaafiliación]
)
VALUES
    (1, 1, '2023-07-14 09:30:00'),
    (2, 2, '2023-07-15 14:45:00')
GO
