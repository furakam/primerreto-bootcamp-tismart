﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace SeguroMVC.Models;

public partial class BdsegurosContext : DbContext
{
    public BdsegurosContext()
    {
    }

    public BdsegurosContext(DbContextOptions<BdsegurosContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Tmcliente> Tmclientes { get; set; }

    public virtual DbSet<TmcompaniaAseguradora> TmcompaniaAseguradoras { get; set; }

    public virtual DbSet<Tmseguro> Tmseguros { get; set; }

    public virtual DbSet<Trafiliacion> Trafiliacions { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseSqlServer("Server=Furakam; Database=BDSeguros; Trusted_Connection=True; TrustServerCertificate=True");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Tmcliente>(entity =>
        {
            entity.HasKey(e => e.Nclienteid).HasName("PK__TMClient__C1873A7C0C463446");

            entity.ToTable("TMCliente");

            entity.Property(e => e.Nclienteid).HasColumnName("nclienteid");
            entity.Property(e => e.Cclienteapellidomaterno)
                .HasMaxLength(50)
                .HasColumnName("cclienteapellidomaterno");
            entity.Property(e => e.Cclienteapellidopaterno)
                .HasMaxLength(50)
                .HasColumnName("cclienteapellidopaterno");
            entity.Property(e => e.Cclientedireccion)
                .HasMaxLength(200)
                .HasColumnName("cclientedireccion");
            entity.Property(e => e.Cclientedocumentoidentidad)
                .HasMaxLength(20)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("cclientedocumentoidentidad");
            entity.Property(e => e.Cclienteemail)
                .HasMaxLength(100)
                .HasColumnName("cclienteemail");
            entity.Property(e => e.Cclienteestado)
                .HasDefaultValueSql("((1))")
                .HasColumnName("cclienteestado");
            entity.Property(e => e.Cclientegenero)
                .HasMaxLength(1)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("cclientegenero");
            entity.Property(e => e.Cclientenombres)
                .HasMaxLength(100)
                .HasColumnName("cclientenombres");
            entity.Property(e => e.Cclientetelefono)
                .HasMaxLength(9)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("cclientetelefono");
            entity.Property(e => e.Cusercreate)
                .HasMaxLength(50)
                .HasDefaultValueSql("('DEFAULT_USER')")
                .HasColumnName("cusercreate");
            entity.Property(e => e.Cuserupdate)
                .HasMaxLength(50)
                .HasDefaultValueSql("('DEFAULT_USER')")
                .HasColumnName("cuserupdate");
            entity.Property(e => e.Dclientefechanacimiento)
                .HasColumnType("date")
                .HasColumnName("dclientefechanacimiento");
            entity.Property(e => e.Dusercreate)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("dusercreate");
            entity.Property(e => e.Duserupdate)
                .HasColumnType("datetime")
                .HasColumnName("duserupdate");
            entity.Property(e => e.Ntipodocumento)
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasColumnName("ntipodocumento");
        });

        modelBuilder.Entity<TmcompaniaAseguradora>(entity =>
        {
            entity.HasKey(e => e.Ncompaniaid).HasName("PK__TMCompan__D70A48F39BA772C3");

            entity.ToTable("TMCompaniaAseguradora");

            entity.Property(e => e.Ncompaniaid).HasColumnName("ncompaniaid");
            entity.Property(e => e.Ccompaniacelular)
                .HasMaxLength(9)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("ccompaniacelular");
            entity.Property(e => e.Ccompaniacontacto)
                .HasMaxLength(9)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("ccompaniacontacto");
            entity.Property(e => e.Ccompaniacontrato).HasColumnName("ccompaniacontrato");
            entity.Property(e => e.Ccompaniadescripcion)
                .HasMaxLength(1000)
                .HasColumnName("ccompaniadescripcion");
            entity.Property(e => e.Ccompaniaestado)
                .HasDefaultValueSql("((1))")
                .HasColumnName("ccompaniaestado");
            entity.Property(e => e.Ccompaniarazonsocial)
                .HasMaxLength(400)
                .HasColumnName("ccompaniarazonsocial");
            entity.Property(e => e.Ccompaniaruc)
                .HasMaxLength(11)
                .IsUnicode(false)
                .IsFixedLength()
                .HasColumnName("ccompaniaruc");
            entity.Property(e => e.Cusercreate)
                .HasMaxLength(50)
                .HasDefaultValueSql("('DEFAULT_USER')")
                .HasColumnName("cusercreate");
            entity.Property(e => e.Cuserupdate)
                .HasMaxLength(50)
                .HasDefaultValueSql("('DEFAULT_USER')")
                .HasColumnName("cuserupdate");
            entity.Property(e => e.Dcompaniafecharenovacion)
                .HasColumnType("datetime")
                .HasColumnName("dcompaniafecharenovacion");
            entity.Property(e => e.Dusercreate)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("dusercreate");
            entity.Property(e => e.Duserupdate)
                .HasColumnType("datetime")
                .HasColumnName("duserupdate");
        });

        modelBuilder.Entity<Tmseguro>(entity =>
        {
            entity.HasKey(e => e.Nseguroid).HasName("PK__TMSeguro__E23720B0A84C5548");

            entity.ToTable("TMSeguro");

            entity.Property(e => e.Nseguroid).HasColumnName("nseguroid");
            entity.Property(e => e.Csegurodescripcion)
                .HasMaxLength(1000)
                .HasColumnName("csegurodescripcion");
            entity.Property(e => e.Cseguroestado)
                .HasDefaultValueSql("((1))")
                .HasColumnName("cseguroestado");
            entity.Property(e => e.Cseguronumero)
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasColumnName("cseguronumero");
            entity.Property(e => e.Cusercreate)
                .HasMaxLength(50)
                .HasDefaultValueSql("('DEFAULT_USER')")
                .HasColumnName("cusercreate");
            entity.Property(e => e.Cuserupdate)
                .HasMaxLength(50)
                .HasDefaultValueSql("('DEFAULT_USER')")
                .HasColumnName("cuserupdate");
            entity.Property(e => e.Dsegurofechavigencia)
                .HasColumnType("datetime")
                .HasColumnName("dsegurofechavigencia");
            entity.Property(e => e.Dusercreate)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("dusercreate");
            entity.Property(e => e.Duserupdate)
                .HasColumnType("datetime")
                .HasColumnName("duserupdate");
            entity.Property(e => e.Ncompaniaid).HasColumnName("ncompaniaid");
            entity.Property(e => e.Nmonedaid)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("nmonedaid");
            entity.Property(e => e.Nsegurocobertura)
                .HasColumnType("money")
                .HasColumnName("nsegurocobertura");
            entity.Property(e => e.Nseguroedadmaxima).HasColumnName("nseguroedadmaxima");
            entity.Property(e => e.Nsegurofactorimpuesto).HasColumnName("nsegurofactorimpuesto");
            entity.Property(e => e.Nseguroimportemensual)
                .HasColumnType("money")
                .HasColumnName("nseguroimportemensual");
            entity.Property(e => e.Nseguromontoprima)
                .HasColumnType("money")
                .HasColumnName("nseguromontoprima");
            entity.Property(e => e.Nseguroporcentajecomision).HasColumnName("nseguroporcentajecomision");
            entity.Property(e => e.Ntiposeguro)
                .HasMaxLength(250)
                .IsUnicode(false)
                .HasColumnName("ntiposeguro");

            entity.HasOne(d => d.Ncompania).WithMany(p => p.Tmseguros)
                .HasForeignKey(d => d.Ncompaniaid)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Ncompaniaid");
        });

        modelBuilder.Entity<Trafiliacion>(entity =>
        {
            entity.HasKey(e => e.Nafiliacionid).HasName("PK__TRAfilia__B42D0B1B49C84890");

            entity.ToTable("TRAfiliacion");

            entity.Property(e => e.Nafiliacionid).HasColumnName("nafiliacionid");
            entity.Property(e => e.Cafiliacionestado)
                .HasDefaultValueSql("((1))")
                .HasColumnName("cafiliacionestado");
            entity.Property(e => e.Cusercreate)
                .HasMaxLength(50)
                .HasDefaultValueSql("('DEFAULT_USER')")
                .HasColumnName("cusercreate");
            entity.Property(e => e.Cuserupdate)
                .HasMaxLength(50)
                .HasDefaultValueSql("('DEFAULT_USER')")
                .HasColumnName("cuserupdate");
            entity.Property(e => e.Dsegurofechaafiliación)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("dsegurofechaafiliación");
            entity.Property(e => e.Dusercreate)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("dusercreate");
            entity.Property(e => e.Duserupdate)
                .HasColumnType("datetime")
                .HasColumnName("duserupdate");
            entity.Property(e => e.Nclienteid).HasColumnName("nclienteid");
            entity.Property(e => e.Nseguroid).HasColumnName("nseguroid");

            entity.HasOne(d => d.Ncliente).WithMany(p => p.Trafiliacions)
                .HasForeignKey(d => d.Nclienteid)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Nclienteid");

            entity.HasOne(d => d.Nseguro).WithMany(p => p.Trafiliacions)
                .HasForeignKey(d => d.Nseguroid)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Nseguroid");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
