namespace SeguroMVC.Models.Dto.Afiliacion
{
    public class GetFullTrafiliacion
    {
        public int Nafiliacionid { get; set; }
        public int Nclienteid { get; set; }
        public int Nseguroid { get; set; }
    }
}