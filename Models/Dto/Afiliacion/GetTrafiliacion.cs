
namespace SeguroMVC.Models.Dto.Afiliacion
{
    public class GetTrafiliacion
    {
        public int Nafiliacionid { get; set; }

        public string Cclientedocumentoidentidad { get; set; } = null!;

        public string Cseguronumero { get; set; } = null!;

        public string Dsegurofechaafiliación { get; set; } = null!;
    }
}