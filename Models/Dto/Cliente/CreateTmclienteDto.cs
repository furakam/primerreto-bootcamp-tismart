using System;
using System.Collections.Generic;

namespace SeguroMVC.Models.Dto.Cliente
{
    public class CreateTmclienteDto
    {
        public string Cclientenombres { get; set; } = null!;
        public string Cclienteapellidopaterno { get; set; } = null!;
        public string Cclienteapellidomaterno { get; set; } = null!;
        public string Ntipodocumento { get; set; } = null!;
        public string Cclientedocumentoidentidad { get; set; } = null!;
        public string Cclientegenero { get; set; } = null!;
        public string Cclienteemail { get; set; } = null!;
        public string Cclientetelefono { get; set; } = null!;
        public string Cclientedireccion { get; set; } = null!;
        public DateTime Dclientefechanacimiento { get; set; }
    }
}