namespace SeguroMVC.Models.Dto.Cliente
{
    public class GetFullTmclienteDto
    {
        public int Nclienteid { get; set; }

        public string Ntipodocumento { get; set; } = null!;

        public string Cclientedocumentoidentidad { get; set; } = null!;

        public string Cclientegenero { get; set; } = null!;

        public string Cclienteapellidopaterno { get; set; } = null!;

        public string Cclienteapellidomaterno { get; set; } = null!;

        public string Cclientenombres { get; set; } = null!;

        public string Cclienteemail { get; set; } = null!;

        public string Cclientetelefono { get; set; } = null!;

        public string Cclientedireccion { get; set; } = null!;

        public string Dclientefechanacimiento { get; set; } = string.Empty;
    }
}