
namespace SeguroMVC.Models.Dto.Cliente
{
    public class GetTmclienteDto
    {
        public int Nclienteid { get; set; }
        public string Cclientenombres { get; set; } = null!;
        public string Cclienteapellidopaterno { get; set; } = null!;
        public string Cclienteapellidomaterno { get; set; } = null!;
        public string Cclientedocumentoidentidad { get; set; } = null!;
        public string Cclienteemail { get; set; } = null!;
        public string Cclientetelefono { get; set; } = null!;
        public string Cclientegenero { get; set; } = null!;
    }
}