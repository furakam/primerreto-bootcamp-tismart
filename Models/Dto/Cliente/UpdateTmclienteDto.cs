namespace SeguroMVC.Models.Dto.Cliente
{
    public class UpdateTmclienteDto : CreateTmclienteDto
    {
        public int Nclienteid { get; set; }
    }
}