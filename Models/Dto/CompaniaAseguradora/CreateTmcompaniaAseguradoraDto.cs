namespace SeguroMVC.Models.Dto.CompaniaAseguradora
{
    public class CreateTmcompaniaAseguradoraDto
    {
        public string Ccompaniarazonsocial { get; set; } = null!;
        public string Ccompaniadescripcion { get; set; } = null!;
        public string Ccompaniaruc { get; set; } = null!;
        public string Ccompaniacontacto { get; set; } = null!;
        public string Ccompaniacelular { get; set; } = null!;
        public DateTime Dcompaniafecharenovacion { get; set; }
    }
}