using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeguroMVC.Models.Dto.CompaniaAseguradora
{
    public class GetFullTmcompaniaAseguradoraDto
    {
        public int Ncompaniaid { get; set; }
        public string Ccompaniarazonsocial { get; set; } = null!;
        public string Ccompaniadescripcion { get; set; } = null!;
        public string Ccompaniaruc { get; set; } = null!;
        public string Ccompaniacontacto { get; set; } = null!;
        public string Ccompaniacelular { get; set; } = null!;
        public string Dcompaniafecharenovacion { get; set; } = string.Empty;
    }
}