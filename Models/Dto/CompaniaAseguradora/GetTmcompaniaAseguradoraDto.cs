
namespace SeguroMVC.Models.Dto.CompaniaAseguradora
{
    public class GetTmcompaniaAseguradoraDto
    {
        public int Ncompaniaid { get; set; }
        public string Ccompaniadescripcion { get; set; } = null!;
        public string Ccompaniarazonsocial { get; set; } = null!;
        public string Ccompaniacontacto { get; set; } = null!;
        public string Ccompaniacelular { get; set; } = null!;
        public string Dcompaniafecharenovacion { get; set; } = string.Empty;

    }
}