
namespace SeguroMVC.Models.Dto.CompaniaAseguradora
{
    public class UpdateTmcompaniaAseguradoraDto : CreateTmcompaniaAseguradoraDto
    {
        public int Ncompaniaid { get; set; }
    }
}