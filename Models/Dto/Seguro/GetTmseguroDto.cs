namespace SeguroMVC.Models.Dto.Seguro
{
    public class GetTmseguroDto
    {
        public int Nseguroid { get; set; }
        public string Ccompaniarazonsocial { get; set; } = null!;
        public string Cseguronumero { get; set; } = null!;
        public string Ntiposeguro { get; set; } = null!;
        public string Csegurodescripcion { get; set; } = null!;
        public int? Nsegurofactorimpuesto { get; set; }
        public int Nseguroporcentajecomision { get; set; }
        public decimal Nseguromontoprima { get; set; }
        public string Nmonedaid { get; set; } = null!;
        public int Nseguroedadmaxima { get; set; }
        public string Dsegurofechavigencia { get; set; } = string.Empty;
        public decimal Nseguroimportemensual { get; set; }
        public decimal Nsegurocobertura { get; set; }
    }
}