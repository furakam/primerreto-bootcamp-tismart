using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeguroMVC.Models.Dto.Seguro
{
    public class UpdateTmseguroDto : CreateTmseguroDto
    {
        public int Nseguroid { get; set; }
    }
}