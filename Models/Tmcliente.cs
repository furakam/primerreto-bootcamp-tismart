﻿using System;
using System.Collections.Generic;

namespace SeguroMVC.Models;

public partial class Tmcliente
{
    public int Nclienteid { get; set; }

    public string Ntipodocumento { get; set; } = null!;

    public string Cclientedocumentoidentidad { get; set; } = null!;

    public string Cclientegenero { get; set; } = null!;

    public string Cclienteapellidopaterno { get; set; } = null!;

    public string Cclienteapellidomaterno { get; set; } = null!;

    public string Cclientenombres { get; set; } = null!;

    public string Cclienteemail { get; set; } = null!;

    public string Cclientetelefono { get; set; } = null!;

    public string Cclientedireccion { get; set; } = null!;

    public DateTime Dclientefechanacimiento { get; set; }

    public bool? Cclienteestado { get; set; }

    public string? Cusercreate { get; set; }

    public DateTime Dusercreate { get; set; }

    public string? Cuserupdate { get; set; }

    public DateTime? Duserupdate { get; set; }

    public virtual ICollection<Trafiliacion> Trafiliacions { get; set; } = new List<Trafiliacion>();
}
