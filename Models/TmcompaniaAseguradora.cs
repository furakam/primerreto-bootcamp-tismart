﻿using System;
using System.Collections.Generic;

namespace SeguroMVC.Models;

public partial class TmcompaniaAseguradora
{
    public int Ncompaniaid { get; set; }

    public string Ccompaniadescripcion { get; set; } = null!;

    public string Ccompaniaruc { get; set; } = null!;

    public string Ccompaniarazonsocial { get; set; } = null!;

    public string Ccompaniacontacto { get; set; } = null!;

    public string Ccompaniacelular { get; set; } = null!;

    public string? Ccompaniacontrato { get; set; }

    public DateTime Dcompaniafecharenovacion { get; set; }

    public bool? Ccompaniaestado { get; set; }

    public string? Cusercreate { get; set; }

    public DateTime Dusercreate { get; set; }

    public string? Cuserupdate { get; set; }

    public DateTime? Duserupdate { get; set; }

    public virtual ICollection<Tmseguro> Tmseguros { get; set; } = new List<Tmseguro>();
}
