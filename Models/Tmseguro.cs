﻿using System;
using System.Collections.Generic;

namespace SeguroMVC.Models;

public partial class Tmseguro
{
    public int Nseguroid { get; set; }

    public int Ncompaniaid { get; set; }

    public string Cseguronumero { get; set; } = null!;

    public string Ntiposeguro { get; set; } = null!;

    public string Csegurodescripcion { get; set; } = null!;

    public int? Nsegurofactorimpuesto { get; set; }

    public int Nseguroporcentajecomision { get; set; }

    public decimal Nseguromontoprima { get; set; }

    public string Nmonedaid { get; set; } = null!;

    public int Nseguroedadmaxima { get; set; }

    public DateTime Dsegurofechavigencia { get; set; }

    public decimal Nseguroimportemensual { get; set; }

    public decimal Nsegurocobertura { get; set; }

    public bool? Cseguroestado { get; set; }

    public string? Cusercreate { get; set; }

    public DateTime Dusercreate { get; set; }

    public string? Cuserupdate { get; set; }

    public DateTime? Duserupdate { get; set; }

    public virtual TmcompaniaAseguradora Ncompania { get; set; } = null!;

    public virtual ICollection<Trafiliacion> Trafiliacions { get; set; } = new List<Trafiliacion>();
}
