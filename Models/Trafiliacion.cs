﻿using System;
using System.Collections.Generic;

namespace SeguroMVC.Models;

public partial class Trafiliacion
{
    public int Nafiliacionid { get; set; }

    public int Nclienteid { get; set; }

    public int Nseguroid { get; set; }

    public DateTime? Dsegurofechaafiliación { get; set; }

    public bool? Cafiliacionestado { get; set; }

    public string? Cusercreate { get; set; }

    public DateTime Dusercreate { get; set; }

    public string? Cuserupdate { get; set; }

    public DateTime? Duserupdate { get; set; }

    public virtual Tmcliente Ncliente { get; set; } = null!;

    public virtual Tmseguro Nseguro { get; set; } = null!;
}
