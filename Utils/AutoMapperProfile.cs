using AutoMapper;
using Microsoft.Data.SqlClient;
using SeguroMVC.Models;
using SeguroMVC.Models.Dto.Afiliacion;
using SeguroMVC.Models.Dto.Cliente;
using SeguroMVC.Models.Dto.CompaniaAseguradora;
using SeguroMVC.Models.Dto.Seguro;

namespace SeguroMVC.Utils
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            //CompaniaAseguradora
            CreateMap<TmcompaniaAseguradora, GetTmcompaniaAseguradoraDto>().ForMember(dest => dest.Dcompaniafecharenovacion, opt => opt.MapFrom(src => src.Dcompaniafecharenovacion.ToString("dd/MM/yyyy")));

            CreateMap<TmcompaniaAseguradora, GetFullTmcompaniaAseguradoraDto>().ForMember(dest => dest.Dcompaniafecharenovacion, opt => opt.MapFrom(src => src.Dcompaniafecharenovacion.ToString("yyyy-MM-dd")));

            CreateMap<CreateTmcompaniaAseguradoraDto, TmcompaniaAseguradora>();

            //Cliente
            CreateMap<Tmcliente, GetTmclienteDto>();
            CreateMap<Tmcliente, GetFullTmclienteDto>().ForMember(dest => dest.Dclientefechanacimiento, opt => opt.MapFrom(src => src.Dclientefechanacimiento.ToString("yyyy-MM-dd")));
            CreateMap<CreateTmclienteDto, Tmcliente>();

            //Seguro
            CreateMap<Tmseguro, GetTmseguroDto>()
                .ForMember(dest => dest.Nseguromontoprima, opt => opt.MapFrom(src => Math.Round(src.Nseguromontoprima, 2)))
                .ForMember(dest => dest.Nseguroimportemensual, opt => opt.MapFrom(src => Math.Round(src.Nseguroimportemensual, 2)))
                .ForMember(dest => dest.Nsegurocobertura, opt => opt.MapFrom(src => Math.Round(src.Nsegurocobertura, 2)))
                .ForMember(dest => dest.Ccompaniarazonsocial, opt => opt.MapFrom(src => src.Ncompania.Ccompaniarazonsocial))
                .ForMember(dest => dest.Dsegurofechavigencia, opt => opt.MapFrom(src => src.Dsegurofechavigencia.ToString("dd/MM/yyyy")));

            CreateMap<Tmseguro, GetFullTmsegurodto>()
                .ForMember(dest => dest.Nseguromontoprima, opt => opt.MapFrom(src => Math.Round(src.Nseguromontoprima, 2)))
                .ForMember(dest => dest.Nseguroimportemensual, opt => opt.MapFrom(src => Math.Round(src.Nseguroimportemensual, 2)))
                .ForMember(dest => dest.Nsegurocobertura, opt => opt.MapFrom(src => Math.Round(src.Nsegurocobertura, 2)))
                .ForMember(dest => dest.Dsegurofechavigencia, opt => opt.MapFrom(src => src.Dsegurofechavigencia.ToString("yyy-MM-dd")));
            CreateMap<CreateTmseguroDto, Tmseguro>();

            //Afiliacion
            CreateMap<Trafiliacion, GetTrafiliacion>()
                .ForMember(dest => dest.Cclientedocumentoidentidad, opt => opt.MapFrom(src => src.Ncliente.Cclientedocumentoidentidad))
                .ForMember(dest => dest.Cseguronumero, opt => opt.MapFrom(src => src.Nseguro.Cseguronumero));

            CreateMap<Trafiliacion, GetFullTrafiliacion>();
            CreateMap<CreateTrafiliacion, Trafiliacion>();
        }
    }
}