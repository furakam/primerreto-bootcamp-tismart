import { enviarDatos } from "./transacciones.js";
import { listarAfiliaciones, fillComboSeguros, fillComboCliente } from "./listar.js";

//Limpiar datos del formulario
function clearData() {
  const elements = $(".clear-data")
  elements.val("");
  elements.removeClass("error")

}

//Funciones Globales

window.abrirModal = function(id){
  clearData();
  $('#modal-form').validate().resetForm()
  if (id !== undefined) {
    $.get(
      `Afiliacion/GetAfiliacionById/${id}`,
      function (data) {
        $.each(data.data, function (key, value) {
          $(`#modal-form #${key}`).val(value)
        });
      }
    );
  }
};

window.eliminar = function(id) {
  if (id !== undefined) {
    if (confirm("Estas seguro de eliminar el registro?")) {
      $.ajax({
        url: `/Afiliacion/DeleteAfiliacionById/${id}`,
        type: "DELETE",
        processData: false,
        contentType: false,
        success: function (datos) {
          if (datos.success) {
            listarAfiliaciones();
          } else {
            alert(datos.message);
          }
        },
      });
    }
  }
}


listarAfiliaciones()
fillComboSeguros()
fillComboCliente()

// Asignando el evento de enviar datos al elemento btnEnviarDatos
$("#btnEnviarDatos").click(enviarDatos);