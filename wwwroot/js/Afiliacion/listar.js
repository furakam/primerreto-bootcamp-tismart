import { generateTable } from "../generate-table.js";

export function listarAfiliaciones() {
  const columns = [
    "Id",
    "Documento de cliente",
    "Número de seguro",
    "Fecha de afiliación",
    "Acciones"
  ]
   $.get("/Afiliacion/ListarAfiliaciones", function (data) {
    generateTable(columns, data.data, "table");
  });
}

export function fillComboSeguros() {
  $.get("/Seguro/ListarSeguros", function (response) {
    const combo = $('#nseguroid')
    let contenido = `<option value="">Seleccione...</option>`
    response.data.forEach(d => {
      contenido += `<option value="${d.nseguroid}">${d.cseguronumero} - ${d.ntiposeguro}</option>`
    });
    combo.html(contenido)
  });
}

export function fillComboCliente() {
  $.get("/Cliente/ListarClientes", function (response) {
    const combo = $('#nclienteid')
    let contenido = `<option value="">Seleccione...</option>`
    response.forEach(d => {
      contenido += `<option value="${d.nclienteid}">${d.cclientenombres} ${d.cclienteapellidopaterno} ${d.cclienteapellidomaterno} - ${d.cclientedocumentoidentidad}</option>`
    });
    combo.html(contenido)
  });
}