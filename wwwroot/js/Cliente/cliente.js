import { listarClientes } from "./listar.js";
import { enviarDatos } from "./transacciones.js";

function clearData() {
  const elements = $(".clear-data")
  elements.val("");
  elements.removeClass("error")

}

window.abrirModal = function (id) {
  clearData();
  $('#modal-form').validate().resetForm()
  if (id !== undefined) {
    $.get(
      `Cliente/GetClienteById/${id}`,
      function (data) {
        $.each(data.data, function (key, value) {
          $(`#modal-form #${key}`).val(value)
        });
      }
    );
  }
};

window.eliminar = function(id) {
  if (id !== undefined) {
    if (confirm("Estas seguro de eliminar el registro?")) {
      $.ajax({
        url: `/Cliente/DeleteClienteById/${id}`,
        type: "DELETE",
        processData: false,
        contentType: false,
        success: function (datos) {
          if (datos.success) {
            listarClientes();
          } else {
            alert(datos.message);
          }
        },
      });
    }
  }
}

listarClientes()

// Asignando el evento de enviar datos al elemento btnEnviarDatos
$("#btnEnviarDatos").click(enviarDatos);