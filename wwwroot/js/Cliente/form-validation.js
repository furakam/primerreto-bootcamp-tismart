$(document).ready(function () {
  $("#modal-form").validate({
    rules: {
      ntipodocumento: {
        required: true,
      },
      cclientedocumentoidentidad: {
        minlength: 8,
        maxlength: 20,
        required: true,
      },
      cclientegenero: {
        required: true,
      },
      cclienteapellidopaterno: {
        required: true,
      },
      cclienteapellidomaterno: {
        required: true,
      },
      cclientenombres: {
        required: true,
      },
      cclienteemail: {
        email: true,
        required: true,
      },
      cclientetelefono: {
        minlength: 9,
        maxlength: 9,
        required: true,
      },
      cclientedireccion: {
        required: true,
      },
      dclientefechanacimiento: {
        required: true,
      },
    },
    messages: {
      ntipodocumento: {
        required: "Este campo es requerido",
      },
      cclientedocumentoidentidad: {
        minlength: "El documento debe tener como mínimo 8 caracteres",
        maxlength: "El documento debe tener como máximo 20 caracteres",
        required: "Este campo es requerido",
      },
      cclientegenero: {
        required: "Este campo es requerido",
      },
      cclienteapellidopaterno: {
        required: "Este campo es requerido",
      },
      cclienteapellidomaterno: {
        required: "Este campo es requerido",
      },
      cclientenombres: {
        required: "Este campo es requerido",
      },
      cclienteemail: {
        email: "Debe ingresar un email válido",
        required: "Este campo es requerido",
      },
      cclientetelefono: {
        minlength: "El documento debe tener como mínimo 9 caracteres",
        maxlength: "El documento debe tener como máximo 9 caracteres",
        required: "Este campo es requerido",
      },
      cclientedireccion: {
        required: "Este campo es requerido",
      },
      dclientefechanacimiento: {
        required: "Este campo es requerido",
      },
    },
  });
});
