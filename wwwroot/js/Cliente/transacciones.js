import { listarClientes } from "./listar.js";

export function enviarDatos() {
  //Comprobando que el formulario contenga los datos válidos
  var isValidate = $("#modal-form").valid();

  if (isValidate) {
    const id = $("#nclienteid").val();

    //Obteniendo todos los datos del formulario
    const dataform = new FormData($("#modal-form")[0]);
    if (id === undefined || id === "") {
      $.ajax({
        url: "/Cliente/RegistrarCliente",
        type: "POST",
        data: dataform,
        processData: false,
        contentType: false,
        success: function (datos) {
          if (datos.success) {
            $("#btnCloseModal").trigger("click"); //Haciendo click en el botón de cerrar modal
            listarClientes();
          } else {
            alert(datos.message);
          }
        },
      });
    } else {
      $.ajax({
        url: "/Cliente/ActualizarCliente",
        type: "PUT",
        data: dataform,
        processData: false,
        contentType: false,
        success: function (datos) {
          if (datos.success) {
            $("#btnCloseModal").trigger("click"); //Haciendo click en el botón de cerrar modal
            listarClientes();
          } else {
            alert(datos.message);
          }
        },
      });
    }
  }
}
