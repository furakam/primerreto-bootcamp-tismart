import { generateTable } from "../generate-table.js";

/**
 * Esta funcion realiza el listado de datos en la tabla de Compañía aseguradora
 */
export function listarCompaniaSeguro() {
  //Listado de tabla
  const columns = [
    "Id",
    "Descripción",
    "Razon social",
    "Contacto",
    "Celular",
    "Fecha renovación",
    "Acciones",
  ];
  $.get("/CompaniaAseguradora/ListarCompaniaAseguradora", function (data) {
    generateTable(columns, data, "table");
  });
}
