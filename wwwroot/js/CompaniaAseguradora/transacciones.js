import { listarCompaniaSeguro } from "./listar.js";

export function enviarDatos() {
  //Comprobando que el formulario contenga los datos válidos
  var isValidate = $("#modal-form").valid();

  if (isValidate) {
    const id = $("#ncompaniaid").val();

    //Obteniendo todos los datos del formulario
    const dataform = new FormData($("#modal-form")[0]);
    if (id === undefined || id === "") {
      $.ajax({
        url: "/CompaniaAseguradora/RegistrarCompaniaAseguradora",
        type: "POST",
        data: dataform,
        processData: false,
        contentType: false,
        success: function (datos) {
          if (datos.success) {
            $("#btnCloseModal").trigger("click"); //Haciendo click en el botón de cerrar modal
            listarCompaniaSeguro();
          }
        },
        error: function ({ responseJSON }, status, error) {
          alert(responseJSON.message);
        },
      });
    } else {
      $.ajax({
        url: "/CompaniaAseguradora/ActualizarCompaniaAseguradora",
        type: "PUT",
        data: dataform,
        processData: false,
        contentType: false,
        success: function (datos) {
          if (datos.success) {
            $("#btnCloseModal").trigger("click"); //Haciendo click en el botón de cerrar modal
            listarCompaniaSeguro();
          }
        },
        error: function ({ responseJSON }, status, error) {
          alert(responseJSON.message);
        },
      });
    }
  }
}
