$(document).ready(function () {
  $("#modal-form").validate({
    rules: {
      ncompaniaid: {
        required: true,
      },
      cseguronumero: {
        required: true,
      },
      ntiposeguro: {
        required: true,
      },
      csegurodescripcion: {
        required: true,
      },
      nsegurofactorimpuesto: {
        required: true,
      },
      nseguroporcentajecomision: {
        required: true,
      },
      nseguromontoprima: {
        required: true,
      },
      nmonedaid: {
        required: true,
      },
      nseguroedadmaxima: {
        required: true,
      },
      dsegurofechavigencia: {
        required: true,
      },
      nseguroimportemensual: {
        required: true,
      },
      nsegurocobertura: {
        required: true,
      },
    },
    messages: {
      ncompaniaid: {
        required: "Este campo es requerido",
      },
      cseguronumero: {
        required: "Este campo es requerido",
      },
      ntiposeguro: {
        required: "Este campo es requerido",
      },
      csegurodescripcion: {
        required: "Este campo es requerido",
      },
      nsegurofactorimpuesto: {
        required: "Este campo es requerido",
      },
      nseguroporcentajecomision: {
        required: "Este campo es requerido",
      },
      nseguromontoprima: {
        required: "Este campo es requerido",
      },
      nseguroedadmaxima: {
        required: "Este campo es requerido",
      },
      dsegurofechavigencia: {
        required: "Este campo es requerido",
      },
      dsegurofechavigencia: {
        required: "Este campo es requerido",
      },
      nseguroimportemensual: {
        required: "Este campo es requerido",
      },
      nsegurocobertura: {
        required: "Este campo es requerido",
      },
    },
  });
});