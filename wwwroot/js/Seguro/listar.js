import { generateTable } from "../generate-table.js";

export function listarSeguros() {
  const columns = [
    "Id",
    "Compañía",
    "Número",
    "Tipo",
    "Descripción",
    "Impuesto",
    "Comición (%)",
    "Prima",
    "Moneda",
    "Edad Max.",
    "Vigencia",
    "Importe Mensual",
    "Cobertura",
    "Acciones"
  ]
   $.get("/Seguro/ListarSeguros", function (data) {
    generateTable(columns, data.data, "table");
  });
}

export function fillComboCompanias() {
  $.get("/CompaniaAseguradora/ListarCompaniaAseguradora", function (data) {
    const comboCompania = $('#ncompaniaid')
    let contenido = `<option value="">Seleccione...</option>`
    data.forEach(compania => {
      contenido += `<option value="${compania.ncompaniaid}">${compania.ccompaniarazonsocial}</option>`
    });
    comboCompania.html(contenido)
  });
}