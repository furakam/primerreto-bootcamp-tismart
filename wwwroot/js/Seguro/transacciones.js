import { listarSeguros } from "./listar.js";

export function enviarDatos() {
  //Comprobando que el formulario contenga los datos válidos
  var isValidate = $("#modal-form").valid();

  if (isValidate) {
    const id = $("#nseguroid").val();

    //Obteniendo todos los datos del formulario
    const dataform = new FormData($("#modal-form")[0]);
    if (id === undefined || id === "") {
      $.ajax({
        url: "/Seguro/RegistrarSeguro",
        type: "POST",
        data: dataform,
        processData: false,
        contentType: false,
        success: function (datos) {
          if (datos.success) {
            console.log(datos);
            $("#btnCloseModal").trigger("click"); //Haciendo click en el botón de cerrar modal
            listarSeguros();
          }
        },
        error: function({responseJSON}, status, error){
          alert(responseJSON.message)
        }
      });
    } else {
      $.ajax({
        url: "/Seguro/ActualizarSeguro",
        type: "PUT",
        data: dataform,
        processData: false,
        contentType: false,
        success: function (datos) {
          if (datos.success) {
            console.log(datos);
            $("#btnCloseModal").trigger("click"); //Haciendo click en el botón de cerrar modal
            listarSeguros();
          }
        },
        error: function({responseJSON}, status, error){
          alert(responseJSON.message)
        }
      });
    }
  }
}

